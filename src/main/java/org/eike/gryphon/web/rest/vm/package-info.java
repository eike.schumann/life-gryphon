/**
 * View Models used by Spring MVC REST controllers.
 */
package org.eike.gryphon.web.rest.vm;
