/**
 * Data Access Objects used by WebSocket services.
 */
package org.eike.gryphon.web.websocket.dto;
