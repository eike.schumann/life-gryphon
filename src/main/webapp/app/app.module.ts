import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { LifeGryphonSharedModule } from 'app/shared/shared.module';
import { LifeGryphonCoreModule } from 'app/core/core.module';
import { LifeGryphonAppRoutingModule } from './app-routing.module';
import { LifeGryphonHomeModule } from './home/home.module';
import { LifeGryphonEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    LifeGryphonSharedModule,
    LifeGryphonCoreModule,
    LifeGryphonHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    LifeGryphonEntityModule,
    LifeGryphonAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class LifeGryphonAppModule {}
